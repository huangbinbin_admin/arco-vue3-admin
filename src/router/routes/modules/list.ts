import {DEFAULT_LAYOUT} from '../base';
import {AppRouteRecordRaw} from '../types';

const LIST: AppRouteRecordRaw = {
    path: '/list',
    name: 'list',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'cesium',
        requiresAuth: true,
        icon: 'icon-bytedance-color',
        order: 2,
        roles: ['*'],
    },
    children: [
        {
            path: 'demo', // The midline path complies with SEO specifications
            name: 'demo',
            component: () => import('@/views/list/demo.vue'),
            meta: {
                locale: '原生Cesium',
                requiresAuth: true,
                ignoreCache: false
            },
        },
        {
            path: 'vue-cesium', // The midline path complies with SEO specifications
            name: 'vue-cesium',
            component: () => import('@/views/list/vue-cesium.vue'),
            meta: {
                locale: 'vue-cesium',
                requiresAuth: true,
                ignoreCache: false
            },
        }
    ],
};
export default LIST;
